#include <iostream>
#include <unordered_map>
using namespace std;

/*ЛЕГКОВАГОВИК ДЛЯ ТОВАРІВ*/

struct SharedState
{
    std::string brand_;
    std::string country_;
    std::string color_;

    SharedState(const std::string &brand, const std::string &country, const std::string &color)
        : brand_(brand), country_(country), color_(color)
    {
    }

    friend std::ostream &operator<<(std::ostream &os, const SharedState &ss)
    {
        return os << "[ " << ss.brand_ << " , " << ss.country_ << " , " << ss.color_ << " ]";
    }
};

struct UniqueState
{
    std::string type_;
    std::string storecode_;

    UniqueState(const std::string &type, const std::string &storecode)
        : type_(type), storecode_(storecode)
    {
    }

    friend std::ostream &operator<<(std::ostream &os, const UniqueState &us)
    {
        return os << "[ " << us.type_ << " , " << us.storecode_ << " ]";
    }
};

/**
 * The Flyweight stores a common portion of the state (also called intrinsic
 * state) that belongs to multiple real business entities. The Flyweight accepts
 * the rest of the state (extrinsic state, unique for each entity) via its
 * method parameters.
 */
class Flyweight
{
private:
    SharedState *shared_state_;

public:
    Flyweight(const SharedState *shared_state) : shared_state_(new SharedState(*shared_state))
    {
    }
    Flyweight(const Flyweight &other) : shared_state_(new SharedState(*other.shared_state_))
    {
    }
    ~Flyweight()
    {
        delete shared_state_;
    }
    SharedState *shared_state() const
    {
        return shared_state_;
    }
    void Operation(const UniqueState &unique_state) const
    {
        std::cout << "Flyweight: Displaying shared (" << *shared_state_ << ") and unique (" << unique_state << ") state.\n";
    }
};
/**
 * The Flyweight Factory creates and manages the Flyweight objects. It ensures
 * that flyweights are shared correctly. When the client requests a flyweight,
 * the factory either returns an existing instance or creates a new one, if it
 * doesn't exist yet.
 */
class FlyweightFactory
{
    /**
     * @var Flyweight[]
     */
private:
    std::unordered_map<std::string, Flyweight> flyweights_;
    /**
     * Returns a Flyweight's string hash for a given state.
     */
    std::string GetKey(const SharedState &ss) const
    {
        return ss.brand_ + "_" + ss.country_ + "_" + ss.color_;
    }

public:
    FlyweightFactory(std::initializer_list<SharedState> share_states)
    {
        for (const SharedState &ss : share_states)
        {
            this->flyweights_.insert(std::make_pair<std::string, Flyweight>(this->GetKey(ss), Flyweight(&ss)));
        }
    }

    /**
     * Returns an existing Flyweight with a given state or creates a new one.
     */
    Flyweight GetFlyweight(const SharedState &shared_state)
    {
        std::string key = this->GetKey(shared_state);
        if (this->flyweights_.find(key) == this->flyweights_.end())
        {
            std::cout << "FlyweightFactory: Can't find a flyweight, creating new one.\n";
            this->flyweights_.insert(std::make_pair(key, Flyweight(&shared_state)));
        }
        else
        {
            std::cout << "FlyweightFactory: Reusing existing flyweight.\n";
        }
        return this->flyweights_.at(key);
    }
    void ListFlyweights() const
    {
        size_t count = this->flyweights_.size();
        std::cout << "\nFlyweightFactory: I have " << count << " flyweights:\n";
        for (std::pair<std::string, Flyweight> pair : this->flyweights_)
        {
            std::cout << pair.first << "\n";
        }
    }
};

// ...
void AddFurnitureToDatabase(
    FlyweightFactory &ff, const std::string &storeid, const std::string &type,
    const std::string &brand, const std::string &country, const std::string &color)
{
    std::cout << "\nClient: Adding furniture to database.\n";
    const Flyweight &flyweight = ff.GetFlyweight({brand, country, color});
    // The client code either stores or calculates extrinsic state and passes it
    // to the flyweight's methods.
    flyweight.Operation({storeid, type});
}

/**
 * The client code usually creates a bunch of pre-populated flyweights in the
 * initialization stage of the application.
 */

int main()
{
    FlyweightFactory *factory = new FlyweightFactory({{"MebelArt", "Ukraine", "Black"}, {"LuxMebel", "Italy", "white"}, {"Merce", "Germany", "Brown"}, {"Beretta", "Sweden", "red"}});
    factory->ListFlyweights();

    AddFurnitureToDatabase(*factory,
                            "CL234IR",
                            "Closet",
                            "Jamedoe",
                            "Ukraine",
                            "red");

    AddFurnitureToDatabase(*factory,
                            "CL234IR",
                            "Drawer",
                            "karel",
                            "Spain",
                            "Grey");
    factory->ListFlyweights();
    delete factory;

    return 0;
}
